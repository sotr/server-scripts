#!/bin/bash

#A not particularly smart script to convert raw datacube files to miniSEED
#It converts all files in the folder corresponding to the date provide with -d 
#(even if they have already been converted)

while getopts "u:d:" flag; do
    case "${flag}" in
        u) GATEWAY_USER=${OPTARG};;
        d) CONVERT_DATE=${OPTARG};;
    esac
done
echo "Username: $GATEWAY_USER";

if [ -z $GATEWAY_USER ]; then
    echo Error, no username specified. See example:
    echo ./convert_datcube.sh -u sotr01
    exit 1
fi
if [ -z $CONVERT_DATE ]; then
    echo No convert date was specified with e.g. -d 2021-01-01
    echo Using the default of "'today'"
    CONVERT_DATE="today"
fi

LOG_DATE="`date -u +%Y-%m-%d_%H:%M:%S`"

#Use for running once per day at midnight
CONVERT_DATE_CUBE="`date -u +%y%m%d --date=$CONVERT_DATE`" 
CONVERT_DATE_ISO="`date -u +%Y-%m-%d --date=$CONVERT_DATE`"

echo "$LOG_DATE Starting convert-datacube.sh for $GATEWAY_USER"

CUBETOOLS_DIR="/home/geossotr/server-scripts/cubetools-2021.168/bin"
RAW_DIR="/web/geossotr/public_html/datacube-data/$GATEWAY_USER/"
MSEED_DIR="/web/geossotr/public_html/datacube-data/$GATEWAY_USER/miniSEED"

echo "Converting raw datacube folder dated $CONVERT_DATE_CUBE"
echo "Output Directory: $MSEED_DIR/$CONVERT_DATE_ISO"
 
mkdir -p $MSEED_DIR/$CONVERT_DATE_ISO
$CUBETOOLS_DIR/cube2mseed $RAW_DIR/$CONVERT_DATE_CUBE --output-dir=$MSEED_DIR/$CONVERT_DATE_ISO --force-overwrite -v --fringe-samples=NOMINAL


LOG_DATE="`date -u +%Y-%m-%d_%H:%M:%S`"
echo $LOG_DATE Script complete

