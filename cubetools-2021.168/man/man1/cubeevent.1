'\" t
.\"     Title: cubeevent
.\"    Author: [see the "AUTHOR(S)" section]
.\" Generator: Asciidoctor 2.0.10
.\"      Date: 2021-06-17
.\"    Manual: GIPPtools Command Manual
.\"    Source: Release 2021.168
.\"  Language: English
.\"
.TH "CUBEEVENT" "1" "2021-06-17" "Release 2021.168" "GIPPtools Command Manual"
.ie \n(.g .ds Aq \(aq
.el       .ds Aq '
.ss \n[.ss] 0
.nh
.ad l
.de URL
\fI\\$2\fP <\\$1>\\$3
..
.als MTO URL
.if \n[.g] \{\
.  mso www.tmac
.  am URL
.    ad l
.  .
.  am MTO
.    ad l
.  .
.  LINKSTYLE blue R < >
.\}
.SH "NAME"
cubeevent \- list events captured by the Cube "event recorder" hardware
.SH "SYNOPSIS"
.sp
.sp
.nf
cubeevent  [\-v|\-\-verbose] [\-\-include\-pattern=\fIPATTERN\fP]...
           [\-\-output\-dir=\fIDIRECTORY\fP [\-\-force\-overwrite]]
           [\-\-format=\fIFORMAT\fP]
           \fIfile\fP | \fIdirectory\fP...
.fi
.br
.sp
.sp
.nf
cubeevent  [\-h|\-\-help] [\-\-version] [\-\-sysinfo]
.fi
.br
.SH "DESCRIPTION"
.sp
An "event recorder" is a specialized Cube data logger variant designed
to record the precise time of discrete (seismic) events. Some typical
sources for these events are sledge hammer, drop weight or explosives.
The \fBcubeevent\fP program is used to readout information from "event
recorder" files.
.sp
Calling \fBcubeevent\fP with one or more "event recorder" files as argument
will by default return a short list of all recorded and detected events
in those files. If a \fIdirectory\fP is given at the command line,
\fBcubeevent\fP will search recursively for input files inside that
directory. The report is by default written to standard output (i.e.
console) or saved in an output directory (use option \fB\-\-output\-dir\fP).
Different report variants (EVENTS, BATTERY, ...) are available and can
be selected using the \fB\-\-format\fP option.
.if n .sp
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
.B Note
.ps -1
.br
.sp
The files written by "event recorder" hardware are essentially just a
specialized variant of the "normal" Cube data logger file format. It is
therefore feasible to use other GIPPtools such as e.g. \fBcubeinfo\fP or
\fBcube2ascii\fP to access the contained time series data as well. However,
only the \fBcubeevent\fP utility provides complete access to the extra
information (e.g. automatically detected events or battery voltage)
contained in "event recorder" files.
.sp .5v
.RE
.SH "OPTIONS"
.sp
The program pretty much follows expected Unix command line syntax. Some
of the command line options have two variants, one long and an
additional short one (for convenience). These are shown below, separated
by commas. However, most options only have a long variant. The ‘=’ for
options that take a parameter is required and can not be replaced by a
whitespace.
.sp
\-h, \-\-help
.RS 4
Print a brief summary of all available command line options and exit.
.RE
.sp
\-\-version
.RS 4
Print the \fBcubeevent\fP release information and exit.
.RE
.sp
\-\-sysinfo
.RS 4
Provide some basic system information and exit.
.RE
.sp
\-v, \-\-verbose
.RS 4
This option increases the amount of information given to the user during
the program execution. By default (i.e. without this option) \fBcubeevent\fP
only reports warnings and errors. (See the diagnostics section below.)
.RE
.sp
\-\-include\-pattern=\fIPATTERN\fP
.RS 4
Only read data from event recorder files whose filename matches the
given \fIPATTERN\fP. Files with a name not matching the search \fIPATTERN\fP
will be ignored. This option is quite useful to speed up recursive
searches through large subdirectory trees and can be used more than once
in the same command line.
.sp
You can use the two wild card characters ( *, ?) when specifying a
PATTERN (e.g. *.Q12). Or alternatively, you can also use a predefined
filter called GIPP that can be used to exclude all files not following
the usual GIPP naming convention for files recorded by "event recorder"
hardware.
.if n .sp
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
.B Caution
.ps -1
.br
.sp
The given search \fIPATTERN\fP is only applied to the filename part
and not to the full pathname of a file.
.sp .5v
.RE
.RE
.sp
\-\-output\-dir=\fIDIRECTORY\fP
.RS 4
Save the resulting reports to this \fIDIRECTORY\fP. The directory must
already exist and be writable! Already existing files will not be
overwritten unless the option \fB\-\-force\-overwrite\fP is used as well.
.RE
.sp
\fB\-\-force\-overwrite\fP
If this option is used, already existing files in the output directory
will be overwritten without mercy!
.sp
+
The default behavior however is \fBnot\fP to overwrite already existing
files. Instead a new file is created with an additional number in
between filename and extension.
.sp
\fB\-\-format=\fP\fIFORMAT\fP
.sp
+
.sp
Select one of the following predefined output formats:
.sp
EVENTS
.RS 4
List all recorded/triggered events. The output consists of a sequential
event number, the event time and information about the age of the GPS
fix that was used to determine the event time. Example:
.sp
.if n .RS 4
.nf
# \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-
# recording unit: c0000     file name: 03301038.000
# \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-
Event #1  2017\-03\-30T10:39:00.795750  (GPS ok)
Event #2  2017\-03\-30T10:41:00.000031  (GPS 14s old)
Event #3  2017\-03\-30T10:43:00.000313  (GPS ok)
.fi
.if n .RE
.sp
Events are simply numbered in the order they were read from the file
input. The information about the age of the GPS fix allows a rough
assessment of the GPS reception during the recording.
.if n .sp
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
.B Note
.ps -1
.br
.sp
This utility does not provide more detailed GPS information because
this is already available via the \fBcubeinfo\fP program. Simply use the
\fB\-\-format=GPS\fP command line option of the \fBcubeinfo\fP utility.
.sp .5v
.RE
.sp
If the \fB\-\-format\fP command line option is not used, the program will
default to the EVENTS output format!
.RE
.sp
ALL
.RS 4
This mode will output ALL samples recorded by the "event recorder". The
output will consist of the recording time of the sample, the two primary
recording channels (usually the electric signal from the cable used to
trigger the explosion and a seismic signal from a geophone located
nearby the source) as well as the two auxiliary channels tracking the
state of the recording button (1 \- pressed, 0 \- unpressed) and marking
the first sample after the detected event. Example
.sp
.if n .RS 4
.nf
# \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-
# recording unit: c0000     file name: 03301038.000
# \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-
2017\-03\-30T10:39:00.793000    \-195  174  0  0
2017\-03\-30T10:39:00.794000    \-124  \-66  0  0
2017\-03\-30T10:39:00.795000     \-88  476  1  0
2017\-03\-30T10:39:00.796000  \-25122   97  1  0
2017\-03\-30T10:39:00.797000  \-17719   80  1  1
2017\-03\-30T10:39:00.798000  \-29410  421  1  0
2017\-03\-30T10:39:00.799000  \-27118   41  1  0
2017\-03\-30T10:39:00.800000  \-26366  121  0  0
2017\-03\-30T10:39:00.801000  \-25775  313  0  0
.fi
.if n .RE
.RE
.sp
REC
.RS 4
The output format is identical to the ALL format described above.
However, only samples recorded while "recording" button was pressed
(i.e. the value of the fourth column is 1) are written. This will reduce
the returned information by the ALL output format to the "interesting
parts".
.RE
.sp
BATTERY
.RS 4
Report the voltage of the internal battery over time. This is mostly
intended for diagnostic purposes.
.RE
.SH "ENVIRONMENT"
.sp
The following environment variables can optionally be used to influence
the behavior of the various GIPPtool utilities during startup.
.sp
GIPPTOOLS_HOME
.RS 4
This environment variable is used to find the location of the GIPPtools
installation directory. In particular, the Java class files that make up
the GIPPtools are expected to be in the java subdirectory of
\fBGIPPTOOLS_HOME\fP.
.RE
.sp
GIPPTOOLS_JAVA
.RS 4
The utilities of the GIPPtools are written in the programming language
Java and consequently need a Java Runtime Environment (JRE) to execute.
Use this variable to specify the location of the JRE which should be
used.
.RE
.sp
GIPPTOOLS_OPTS
.RS 4
You can use this environment variable for additional fine\-tuning of the
Java runtime environment. This is typically used to set the Java heap
size available to GIPPtool programs.
.RE
.sp
GIPPTOOLS_LEAP
.RS 4
The GIPPtools require up\-to\-date leap second information to correctly
interpret Cube files. Usually, this information is obtained from the
leap\-seconds.list file located in the config subdirectory of the
GIPPtools installation directory. This environment variable can be used
to provide a more up\-to\-date leap second list to GIPPtool programs.
.RE
.sp
It is usually not necessary to define any of those variables as suitable
values should be selected automatically. However, if the automatic
detection build into the start script fails or you need to choose
between different GIPPtool or Java runtime releases installed on your
computer, these environment variables might become quite helpful to
troubleshoot the situation.
.SH "DIAGNOSTICS"
.sp
\fBCubeevent\fP occasional will produce user feedback. In general, user
messages are classified as \fIINFO\fP, \fIWARNING\fP or \fIERROR\fP. The \fIINFO\fP
messages are only displayed when the \fB\-\-verbose\fP command line option
is used. They usually report about the progress of the program run.
.sp
More important are \fIWARNING\fP messages. In general, they warn about
(possible) problems that may influence the output. Although the program
will continue with execution, you certainly should check the results
carefully. You might not have gotten what you (thought you) asked for.
Finally, \fIERROR\fP messages inform about problems that can not be resolved
automatically. Program execution usually stops and the user must fix the
problem first.
.SH "EXIT CODES"
.sp
Use the following program exit codes when calling \fBcubeevent\fP from
scripts or other programs to see if \fBcubeevent\fP finished successfully.
Any non\-zero code indicates an ERROR.
.sp
0
.RS 4
Success.
.RE
.sp
64
.RS 4
Command line syntax or usage error.
.RE
.sp
65
.RS 4
Data format error. (The input was not a valid Cube recording.)
.RE
.sp
66
.RS 4
Input file did not exist or could not be opened.
.RE
.sp
70
.RS 4
Error in internal program logic.
.RE
.sp
4
.RS 4
I/O error.
.RE
.sp
99
.RS 4
Other, unspecified errors.
.RE
.SH "EXAMPLES"
.sp
.RS 4
.ie n \{\
\h'-04' 1.\h'+01'\c
.\}
.el \{\
.  sp -1
.  IP " 1." 4.2
.\}
To obtain a short list of all recorded and detected events contained in
the "event recorder" file called recording.cube, you simply use the
following:
.sp
.if n .RS 4
.nf
cubeevent recording.cube
.fi
.if n .RE
.RE
.SH "FILES"
.sp
$GIPPTOOLS_HOME/bin/cubeevent
.RS 4
The \fBcubeevent\fP "program". Usually just a symbolic link pointing to the
standard GIPPtools start script.
.RE
.sp
$GIPPTOOLS_HOME/bin/gipptools
.RS 4
The GIPPtools start script. Almost all utilities of the GIPPtools
package are started from this shell script.
.RE
.SH "SEE ALSO"
.sp
\fBgipptools\fP(1), \fBcube2ascii\fP(1), \fBcube2mseed\fP(1), \fBcube2segy\fP(1),
\fBcubeinfo\fP(1), \fBmseed2ascii\fP(1), \fBmseed2mseed\fP(1), \fBmseed2pdas\fP(1),
\fBmseed2segy\fP(1), \fBmseedcut\fP(1), \fBmseedinfo\fP(1), \fBmseedrecover\fP(1),
\fBmseedrename\fP(1)
.SH "BUGS AND CAVEATS"
.sp
None so far.