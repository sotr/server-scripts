#!/bin/bash

SHUTDOWN=false
GITPULL=false
IMMEDIATE_SHUTDOWN=false

while getopts "u:sgi" flag; do
    case "${flag}" in
        u) GATEWAY_USER=${OPTARG};;
        s) SHUTDOWN=true ;;
        g) GITPULL=true ;;
        i) IMMEDIATE_SHUTDOWN=true;;
    esac
done

USERNUM=${GATEWAY_USER:(-2)}
STATION_PORT=$(( 64000+($USERNUM-1)*2 ))
LOG_DATE="`date -u +%Y-%m-%d_%H:%M:%S`"
TIMEOUT=120 #seconds
COUNTER=0
TUNNEL=0

mkdir -p /web/geossotr/public_html/datacube-data/logs/$GATEWAY_USER/
mkdir -p /web/geossotr/public_html/datacube-data/$GATEWAY_USER/

echo ""
echo "-----------------------------------------------------------------------"
echo ""
echo "$LOG_DATE Starting pull data script for \
$GATEWAY_USER on port $STATION_PORT"
echo "Username: $GATEWAY_USER"
echo "Shutdown setting: $SHUTDOWN"
echo "Git Pull setting: $GITPULL"
echo "Immediate Shutdown: $IMMEDIATE_SHUTDOWN"
#Ping station to see if it is available, times out after 2 minutes
until [ $COUNTER -gt $(($TIMEOUT/10)) ]
  do
    nc -z sotr.geos.ed.ac.uk $STATION_PORT
    if [ $? -eq 0 ]
      then
        echo Found connected tunnel
        TUNNEL=1
        break
      else
        LOG_DATE="`date -u +%Y-%m-%d_%H:%M:%S`"
        echo $LOG_DATE Tunnel not active on try $COUNTER
    fi
    #echo "Retrying in 10s"
    sleep 10
    ((COUNTER++))
  done

if [ $TUNNEL -eq 0 ]; then
    echo Error finding Station tunnel, timed out after $TIMEOUT seconds
fi

if [ $TUNNEL -eq 1 ]; then
    if $IMMEDIATE_SHUTDOWN; then
        echo Immediate shutdown = $IMMEDIATE_SHUTDOWN, shutting down station
        ssh -a -k pi@sotr.geos.ed.ac.uk -p $STATION_PORT \
        "sudo /home/pi/telemetry-box/do_shutdown.sh"
        exit
    else
        echo "Immediate shutdown = $IMMEDIATE_SHUTDOWN, not doing emergency shutdown" 
    fi


    echo Checking if cube is still mounted
   
    # This is pretty ugly, there is no doubt a better way of finding the GPIO number
  #  GPIO_SEISMIC=$(ssh pi@sotr.geos.ed.ac.uk -p $STATION_PORT \
  #  "grep GPIO_SEISMIC /home/pi/tbox-settings.conf | sed 's/export GPIO_SEISMIC=//'"

    GPIO_SEISMIC=26 #This should really be read from the tbox-settings
    COUNTER=0
    
    until [ $COUNTER -gt $(($TIMEOUT/10)) ]
      do
        LOG_DATE="`date -u +%Y-%m-%d_%H:%M:%S`"
        CUBE_STATUS=$(ssh pi@sotr.geos.ed.ac.uk -p $STATION_PORT "gpio -g read $GPIO_SEISMIC")
   
        if [ $CUBE_STATUS -eq 1 ]; then
            echo $LOG_DATE cube still mounted on try $COUNTER
        else
            echo $LOG_DATE Cube GPIO is logic $CUBE_STATUS, continuing
            break
        fi
      sleep 10
      ((COUNTER++))
      done


    echo Pulling Log with rsync...
    rsync -av -e "ssh -p $STATION_PORT" \
    pi@sotr.geos.ed.ac.uk:/home/pi/log/cube-pull-usb.log \
    /web/geossotr/public_html/datacube-data/logs/$GATEWAY_USER/

    echo Pulling Data with rsync...
    rsync -av --remove-source-files --exclude 'telemetry-backup' --stats \
    --progress -e "ssh -p $STATION_PORT" \
     pi@sotr.geos.ed.ac.uk:/home/pi/data/* \
    /web/geossotr/public_html/datacube-data/$GATEWAY_USER/

    echo Deleting empty directories
    ssh pi@sotr.geos.ed.ac.uk -p $STATION_PORT find /home/pi/data/ -depth -type d -empty -delete

    echo Checking battery Voltage
    Vin=$(ssh -a -k pi@sotr.geos.ed.ac.uk -p $STATION_PORT "/bin/bash --login \
    -c /home/pi/telemetry-box/get_Vin.sh")
    echo $Vin
    echo $Vin >> /web/geossotr/public_html/datacube-data/logs/$GATEWAY_USER/battery.log

    if $GITPULL; then    
        echo "Running GIT pull:"
        ssh -a -k pi@sotr.geos.ed.ac.uk -p $STATION_PORT \
        "git -C /home/pi/telemetry-box/ pull"
    fi

    if $SHUTDOWN; then
        echo data pull complete, shutting down station
        ssh -a -k pi@sotr.geos.ed.ac.uk -p $STATION_PORT \
        "sudo /home/pi/telemetry-box/do_shutdown.sh"
    else
        echo "data pull complete, not shutting down, SHUTDOWN set to $SHUTDOWN" 
    fi
fi
LOG_DATE="`date -u +%Y-%m-%d_%H:%M:%S`"
echo $LOG_DATE Script complete

